﻿using Common;
using Common.Enums;
using Common.Resources;
using DataAccess.UnitofWork;
using log4net;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Services.Services
{
    /// <summary>
    /// Base service class
    /// </summary>
    public abstract class AbstractService : IService
    {
        protected static readonly ILog _logger;
        protected readonly IUnitOfWork _uow;
        protected bool _beginTransaction;

        static AbstractService()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        public AbstractService(IUnitOfWork uow, bool beginTransaction = false)
        {
            _uow = uow;
            _beginTransaction = beginTransaction;
        }

        /// <summary>
        /// beginTra
        /// </summary>
        /// <typeparam name="TOutput"> generic output type which will be custom output class</typeparam>
        /// <param name="beginTransaction"> for transaction setting</param>
        /// <param name="callBack"> main metod that sended as argument which start after based proccesses like logging and transaction, 
        /// and before main logic will finish include catching exceptions and disposing </param>
        /// <returns></returns>
        protected async Task<ContainerResult<TOutput>> ExecuteAsync<TOutput>
           (bool beginTransaction, Func<Task<ContainerResult<TOutput>>> callBack)
        {
            ContainerResult<TOutput> result = new ContainerResult<TOutput>();
            try
            {
                _logger.Info($"Executing process started from this class : {GetType()}");

                _beginTransaction = beginTransaction;

                await BeginAsync();

                result = await callBack();

                if (!result.IsSuccess)
                {
                   await RollBackAsync();

                    _logger.Error($"Error occured from this class : {GetType()} : {result.ErrorList[0].ErrorMessage}");

                    return result;
                }

                await CommitAsync();

                _logger.Info($"Executing process finished from this class : {GetType()}");
            }
            catch (Exception ex)
            {
                result.ErrorList.Add(new Error
                {
                    ErrorCode = ErrorCodes.INTERNAL_ERROR,
                    ErrorMessage = Resource.UNHANDLED_EXCEPTION,
                    StatusCode = ErrorHttpStatuses.INTERNAL
                });

                await RollBackAsync();

                _logger.Error($"Error occured from this class : {GetType()} : {ex}");
            }
            finally
            {
                Dispose();
            }

            return result;
        }
        
        protected async Task BeginAsync()
        {
            if (_beginTransaction)
                await _uow.BeginAsync();
        }
       
        protected async Task<bool> CommitAsync()
        {
            bool result = false;
            try
            {
                if (_beginTransaction) await _uow.CommitAsync();
                result = true;
            }
            catch (Exception ex)
            {
                _logger.Error($"Error occured commit : {ex.ToString()}");
            }
            finally
            {
                _uow.Dispose();
            }
            return result;
        }
       
        protected async Task<bool> RollBackAsync()
        {
            bool result = false;

            try
            {
                if (_beginTransaction) await _uow.RollbackAsync();
                result = true;
            }
            catch (Exception ex)
            {
                _logger.Error($"Error occured rollback : {ex.ToString()}");
            }
            finally
            {
                _uow.Dispose();
            }
            return result;
        }

        #region Disposable Members
        public void Dispose() => _uow.Dispose();
        #endregion
    }
}
