﻿using DataAccess.UnitofWork;
using System.Threading.Tasks;
using Models.Entities;
using System.Collections.Generic;
using Models.Dtos;
using System.Linq;
using Common.Enums;
using Common.Resources;
using Common;
using Models.Parameters.Exam;

namespace Services.Services.ExamService
{
    public class ExamService : AbstractService, IExamService
    {
        public ExamService(IUnitOfWork UoW) : base(UoW) { }

        public async Task<ContainerResult<bool>> AddExam(AddExamInput input)
            => await ExecuteAsync(beginTransaction: false, async () =>
             {
                 ContainerResult<bool> result = new ContainerResult<bool>();

                 Subject subject = await _uow.SubjectRepository().GetAsync(x => x.Code.ToUpper().Trim() == input.SubjectCode.ToUpper().Trim());

                 if (subject == null)
                 {
                     result.ErrorList.Add(new Error
                     {
                         ErrorCode = ErrorCodes.SUBJECT_DOES_NOT_EXIST,
                         ErrorMessage = Resource.SUBJECT_DOES_NOT_EXIST,
                         StatusCode = ErrorHttpStatuses.NOT_FOUND
                     });

                     return result;
                 }

                 Student student = await _uow.StudentRepository().GetAsync(x => x.Id == input.StudentId);

                 if (student == null)
                 {
                     result.ErrorList.Add(new Error
                     {
                         ErrorCode = ErrorCodes.STUDENT_DOES_NOT_EXIST,
                         ErrorMessage = Resource.STUDENT_DOES_NOT_EXIST,
                         StatusCode = ErrorHttpStatuses.NOT_FOUND
                     });

                     return result;
                 }

                 if (student.ClassNumber != subject.ClassNumber)
                 {
                     result.ErrorList.Add(new Error
                     {
                         ErrorCode = ErrorCodes.STUDENT_AND_SUBJECT_CLASS_MUST_BE_SAME,
                         ErrorMessage = Resource.STUDENT_AND_SUBJECT_CLASS_MUST_BE_SAME,
                         StatusCode = ErrorHttpStatuses.FORBIDDEN
                     });

                     return result;
                 }

                 Exam exam = new Exam
                 {
                     StudentId = student.Id,
                     SubjectCode = subject.Code,
                     Score = input.Score,
                     BeginDate = input.BeginDate
                 };

                 await _uow.ExamRepository().AddAsync(exam);
                 await _uow.SaveChangesAsync();

                 result.Output = result.IsSuccess;

                 return result;
             });


        public async Task<ContainerResult<GetExamsOutput>> GetExams()
        => await ExecuteAsync(beginTransaction: false, async () =>
        {
            ContainerResult<GetExamsOutput> result = new ContainerResult<GetExamsOutput>();

            List<ExamDto> exams = new List<ExamDto>(_uow.ExamRepository().GetAll(x => x.Student, x => x.Subject).Select(x => (ExamDto)x));

            result.Output = new GetExamsOutput
            {
                Exams = exams
            };

            await Task.CompletedTask;

            return result;
        });
    }
}
