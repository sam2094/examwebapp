﻿using System.Threading.Tasks;
using Models.Parameters.Exam;

namespace Services.Services.ExamService
{
    public interface IExamService
    {
        Task<ContainerResult<bool>> AddExam(AddExamInput input);
        Task<ContainerResult<GetExamsOutput>> GetExams();
    }
}
