﻿using Common;
using System.Collections.Generic;
using System.Linq;

namespace Services.Services
{
	/// <summary>
	/// Class which include base services returning result and errors
	/// </summary>
	/// <typeparam name="TOutput"></typeparam>
	public class ContainerResult<TOutput>
	{
		public TOutput Output { get; set; }
		public List<Error> ErrorList { get; set; } = new List<Error>();
		public bool IsSuccess => !ErrorList.Any();
	}
}
