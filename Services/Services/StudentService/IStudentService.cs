﻿using System.Threading.Tasks;
using Models.Parameters.Student;

namespace Services.Services.StudentService
{
    public interface IStudentService
    {
        Task<ContainerResult<bool>> AddStudent(AddStudentInput input);
        Task<ContainerResult<GetStudentsOutput>> GetStudents();
    }
}
