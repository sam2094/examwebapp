﻿using DataAccess.UnitofWork;
using Models.Parameters.Student;
using System.Threading.Tasks;
using Models.Entities;
using System.Collections.Generic;
using Models.Dtos;
using System.Linq;

namespace Services.Services.StudentService
{
    public class StudentService : AbstractService, IStudentService
    {
        public StudentService(IUnitOfWork UoW) : base(UoW) { }

        public async Task<ContainerResult<bool>> AddStudent(AddStudentInput input)
            => await ExecuteAsync(beginTransaction: false, async () =>
             {
                 ContainerResult<bool> result = new ContainerResult<bool>();

                 Student student = new Student
                 {
                     Name = input.Name.Trim(),
                     Surname = input.Surname.Trim(),
                     ClassNumber = input.ClassNumber
                 };

                 await _uow.StudentRepository().AddAsync(student);
                 await _uow.SaveChangesAsync();

                 result.Output = result.IsSuccess;

                 return result;
             });


        public async Task<ContainerResult<GetStudentsOutput>> GetStudents()
        => await ExecuteAsync(beginTransaction: false, async () =>
        {
            ContainerResult<GetStudentsOutput> result = new ContainerResult<GetStudentsOutput>();

            List<StudentDto> students = new List<StudentDto>(_uow.StudentRepository().GetAll().Select(x => (StudentDto)x));

            result.Output = new GetStudentsOutput
            {
                Students = students
            };

            await Task.CompletedTask;

            return result;
        });
    }
}
