﻿using DataAccess.UnitofWork;
using System.Threading.Tasks;
using Models.Entities;
using System.Collections.Generic;
using Models.Dtos;
using System.Linq;
using Common.Enums;
using Common.Resources;
using Common;
using Models.Parameters.Subject;

namespace Services.Services.SubjectService
{
    public class SubjectService : AbstractService, ISubjectService
    {
        public SubjectService(IUnitOfWork UoW) : base(UoW) { }

        public async Task<ContainerResult<bool>> AddSubject(AddSubjecInput input)
            => await ExecuteAsync(beginTransaction: false, async () =>
             {
                 ContainerResult<bool> result = new ContainerResult<bool>();

                 if (await _uow.SubjectRepository().IsExistAsync(x => x.Code.ToUpper().Trim() == input.Code.ToUpper().Trim()))
                 {
                     result.ErrorList.Add(new Error
                     {
                         ErrorCode = ErrorCodes.SUBJECT_EXIST,
                         ErrorMessage = Resource.SUBJECT_EXIST,
                         StatusCode = ErrorHttpStatuses.FORBIDDEN
                     });

                     return result;
                 }

                 Subject subject = new Subject
                 {
                     Code = input.Code.ToUpper().Trim(),
                     Name = input.Name.Trim(),
                     ClassNumber = input.ClassNumber,
                     TeacherName = input.TeacherName.Trim(),
                     TeacherSurname = input.TeacherSurname.Trim()
                 };

                 await _uow.SubjectRepository().AddAsync(subject);
                 await _uow.SaveChangesAsync();

                 result.Output = result.IsSuccess;

                 return result;
             });


        public async Task<ContainerResult<GetSubjectsOutput>> GetSubjects()
        => await ExecuteAsync(beginTransaction: false, async () =>
        {
            ContainerResult<GetSubjectsOutput> result = new ContainerResult<GetSubjectsOutput>();

            List<SubjectDto> subjects = new List<SubjectDto>(_uow.SubjectRepository().GetAll().Select(x => (SubjectDto)x));

            result.Output = new GetSubjectsOutput
            {
                Subjects = subjects
            };

            await Task.CompletedTask;

            return result;
        });
    }
}
