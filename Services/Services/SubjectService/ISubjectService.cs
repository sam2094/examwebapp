﻿using System.Threading.Tasks;
using Models.Parameters.Subject;

namespace Services.Services.SubjectService
{
    public interface ISubjectService
    {
        Task<ContainerResult<bool>> AddSubject(AddSubjecInput input);
        Task<ContainerResult<GetSubjectsOutput>> GetSubjects();
    }
}
