﻿using System.ComponentModel.DataAnnotations;

namespace Models.Parameters.Student
{
    public class AddStudentInput : ServiceInput
    {
        [Required]
        [StringLength(30)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [Range(1, 11, ErrorMessage = "Max length of class number must be 11")]
        [Display(Name = "Class number")]
        public int ClassNumber { get; set; }
    }
}
