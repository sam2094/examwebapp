﻿using Models.Dtos;
using System.Collections.Generic;

namespace Models.Parameters.Student
{
    public class GetStudentsOutput : ServiceOutput
    {
        public List<StudentDto> Students { get; set; }
    }
}
