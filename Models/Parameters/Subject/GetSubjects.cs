﻿using Models.Dtos;
using System.Collections.Generic;

namespace Models.Parameters.Subject
{
    public class GetSubjectsOutput : ServiceOutput
    {
        public List<SubjectDto> Subjects { get; set; }
    }
}
