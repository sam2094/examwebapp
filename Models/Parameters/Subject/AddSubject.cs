﻿using System.ComponentModel.DataAnnotations;

namespace Models.Parameters.Subject
{
    public class AddSubjecInput : ServiceInput
    {
        [Required]
        [StringLength(3)]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Range(1, 11, ErrorMessage = "Max length of class number must be 11")]
        [Display(Name = "Class number")]
        public int ClassNumber { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Teacher name")]
        public string TeacherName { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Teacher surname")]
        public string TeacherSurname { get; set; }

    }
}
