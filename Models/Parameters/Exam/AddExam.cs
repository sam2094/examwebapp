﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.Parameters.Exam
{
    public class AddExamInput : ServiceInput
    {
        [Required]
        [StringLength(3)]
        [Display(Name = "Subject (Class #)")]
        public string SubjectCode { get; set; }

        [Required]
        [Display(Name = "Student (Class #)")]
        public int StudentId { get; set; }

        [Required]
        [Range(0, 9, ErrorMessage = "Max length of score must be 9")]
        [Display(Name = "Score")]
        public int Score { get; set; }

        [Required]
        [Display(Name = "Begin date")]
        public DateTime BeginDate { get; set; }
    }
}
