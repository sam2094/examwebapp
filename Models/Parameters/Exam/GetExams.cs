﻿using Models.Dtos;
using System.Collections.Generic;

namespace Models.Parameters.Exam
{
    public class GetExamsOutput : ServiceOutput
    {
        public List<ExamDto> Exams { get; set; }
    }
}
