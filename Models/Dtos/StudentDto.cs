﻿using Models.Entities;

namespace Models.Dtos
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int ClassNumber { get; set; }

        public static implicit operator StudentDto(Student v)
        {
            return new StudentDto
            {
                Id = v.Id,
                Name = v.Name,
                Surname = v.Surname,
                ClassNumber = v.ClassNumber
            };
        }
    }
}