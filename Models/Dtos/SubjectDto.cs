﻿using Models.Entities;

namespace Models.Dtos
{
    public class SubjectDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int ClassNumber { get; set; }
        public string TeacherName { get; set; }
        public string TeacherSurname { get; set; }

        public static implicit operator SubjectDto(Subject v)
        {
            return new SubjectDto
            {
                Code = v.Code,
                Name = v.Name,
                ClassNumber = v.ClassNumber,
                TeacherName = v.TeacherName,
                TeacherSurname = v.TeacherSurname
            };
        }
    }
}