﻿using Models.Entities;
using System;

namespace Models.Dtos
{
    public class ExamDto
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectName { get; set; }
        public string TeacherName { get; set; }
        public string TeacherSurname { get; set; }
        public int ClassNumber { get; set; }
        public int Score { get; set; }
        public DateTime BeginDate { get; set; }

        public static implicit operator ExamDto(Exam v)
        {
            return new ExamDto
            {
                StudentId = v.StudentId,
                StudentName = v.Student?.Name,
                StudentSurname = v.Student?.Surname,
                SubjectCode = v.Subject?.Code,
                SubjectName = v.Subject?.Name,
                TeacherName = v.Subject?.TeacherName,
                TeacherSurname = v.Subject?.TeacherName,
                ClassNumber = v.Student.ClassNumber,
                Score = v.Score,
                BeginDate = v.BeginDate
            };
        }
    }
}
