﻿using System;

namespace Models.Entities
{
    public class Exam : BaseEntity
    {
        public int Id { get; set; }
        public string SubjectCode { get; set; }
        public int StudentId { get; set; }
        public int Score { get; set; }
        public DateTime BeginDate { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Student Student { get; set; }
    }
}
