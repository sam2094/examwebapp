﻿using System.Collections.Generic;

namespace Models.Entities
{
    public class Student : BaseEntity
    {
        public Student()
        {
            Exams = new HashSet<Exam>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int ClassNumber { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }
    }
}
