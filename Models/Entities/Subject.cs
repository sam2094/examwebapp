﻿using System.Collections.Generic;

namespace Models.Entities
{
    public class Subject : BaseEntity
    {
        public Subject()
        {
            Exams = new HashSet<Exam>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int ClassNumber { get; set; }
        public string TeacherName { get; set; }
        public string TeacherSurname { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }
    }
}
