﻿using Models.Entities;

namespace DataAccess.Repositories.ExamRepository
{
    public interface IExamRepository : IRepository<Exam> { }
}
