﻿using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace DataAccess.Repositories.ExamRepository
{
    public class ExamRepository : Repository<Exam>, IExamRepository
    {
        public ExamRepository(DbContext dbContext) : base(dbContext) { }
    }
}
