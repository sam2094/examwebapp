﻿using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace DataAccess.Repositories.StudentRepository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(DbContext dbContext) : base(dbContext) { }
    }
}
