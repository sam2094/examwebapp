﻿using Models.Entities;

namespace DataAccess.Repositories.StudentRepository
{
    public interface IStudentRepository : IRepository<Student> { }
}
