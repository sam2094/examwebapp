﻿using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace DataAccess.Repositories.SubjectRepository
{
    public class SubjectRepository : Repository<Subject>, ISubjectRepository
    {
        public SubjectRepository(DbContext dbContext) : base(dbContext) { }
    }
}
