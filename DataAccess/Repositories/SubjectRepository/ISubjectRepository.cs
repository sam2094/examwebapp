﻿using Models.Entities;

namespace DataAccess.Repositories.SubjectRepository
{
    public interface ISubjectRepository : IRepository<Subject> { }
}
