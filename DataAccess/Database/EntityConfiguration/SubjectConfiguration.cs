﻿using DataAccess.Migrations.Seed;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;

namespace DataAccess.Database.EntityConfiguration
{
	public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
	{
		public void Configure(EntityTypeBuilder<Subject> builder)
		{
			builder.ToTable("Subjects", "dbo");

			builder.HasKey(c => c.Code);

			builder.Property(e => e.Code)
				.IsRequired()
				.HasMaxLength(3);


			builder.Property(e => e.Name)
				.IsRequired()
				.HasMaxLength(30)
				.IsUnicode(true);

			builder.Property(e => e.ClassNumber)
				.IsRequired()
				.HasMaxLength(99);

			builder.Property(e => e.TeacherName)
				.IsRequired()
				.HasMaxLength(20)
				.IsUnicode(true);

			builder.Property(e => e.TeacherSurname)
				.IsRequired()
				.HasMaxLength(20)
				.IsUnicode(true);

			SubjectSeed.Seed(builder);
		}
	}
}
