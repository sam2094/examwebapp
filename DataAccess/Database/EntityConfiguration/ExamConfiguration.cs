﻿using DataAccess.Migrations.Seed;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;

namespace DataAccess.Database.EntityConfiguration
{
    public class ExamConfiguration : IEntityTypeConfiguration<Exam>
	{
		public void Configure(EntityTypeBuilder<Exam> builder)
		{
			builder.ToTable("Exams", "dbo");

			builder.Property(e => e.Id)
				.IsRequired()
				.ValueGeneratedOnAdd();

			builder.Property(e => e.BeginDate)
				.IsRequired();

			builder.Property(e => e.Score)
				.IsRequired()
				.HasMaxLength(9);

			builder.HasOne(e => e.Subject)
				 .WithMany(e => e.Exams)
				 .HasForeignKey(e => e.SubjectCode)
				 .OnDelete(DeleteBehavior.NoAction);

			builder.HasOne(e => e.Student)
				 .WithMany(e => e.Exams)
				 .HasForeignKey(e => e.StudentId)
				 .OnDelete(DeleteBehavior.NoAction);

			ExamSeed.Seed(builder);
		}
	}
}
