﻿using DataAccess.Migrations.Seed;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;

namespace DataAccess.Database.EntityConfiguration
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
	{
		public void Configure(EntityTypeBuilder<Student> builder)
		{
			builder.ToTable("Students", "dbo");

			builder.Property(e => e.Id)
				.IsRequired()
				.ValueGeneratedOnAdd();

			builder.Property(e => e.Name)
				.IsRequired()
				.HasMaxLength(30)
				.IsUnicode(true);

			builder.Property(e => e.Surname)
				.IsRequired()
				.HasMaxLength(30)
				.IsUnicode(true);

			builder.Property(e => e.ClassNumber)
				.IsRequired()
				.HasMaxLength(99);

			StudentSeed.Seed(builder);
		}
	}
}
