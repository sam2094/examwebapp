﻿using Microsoft.EntityFrameworkCore;
using DataAccess.Database.EntityConfiguration;
using Models.Entities;

namespace DataAccess.Database
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options)
			: base(options)
		{
		}
		public virtual DbSet<Student> BranchUsers { get; set; }
		public virtual DbSet<Subject> Certificates { get; set; }
		public virtual DbSet<Exam> Branches { get; set; }


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new StudentConfiguration());
			modelBuilder.ApplyConfiguration(new SubjectConfiguration());
			modelBuilder.ApplyConfiguration(new ExamConfiguration());
		}
	}
}
