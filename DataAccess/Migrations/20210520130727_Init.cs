﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Students",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ClassNumber = table.Column<int>(type: "int", maxLength: 99, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                schema: "dbo",
                columns: table => new
                {
                    Code = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ClassNumber = table.Column<int>(type: "int", maxLength: 99, nullable: false),
                    TeacherName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    TeacherSurname = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Code);
                });

            migrationBuilder.CreateTable(
                name: "Exams",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubjectCode = table.Column<string>(type: "nvarchar(3)", nullable: true),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    Score = table.Column<int>(type: "int", maxLength: 9, nullable: false),
                    BeginDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exams_Students_StudentId",
                        column: x => x.StudentId,
                        principalSchema: "dbo",
                        principalTable: "Students",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Exams_Subjects_SubjectCode",
                        column: x => x.SubjectCode,
                        principalSchema: "dbo",
                        principalTable: "Subjects",
                        principalColumn: "Code");
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Students",
                columns: new[] { "Id", "ClassNumber", "Name", "Surname" },
                values: new object[,]
                {
                    { 1, 11, "Samir", "Həsənov" },
                    { 2, 10, "Ruslan", "Əhmədov" },
                    { 3, 8, "Ramiz", "Hüseynov" }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Subjects",
                columns: new[] { "Code", "ClassNumber", "Name", "TeacherName", "TeacherSurname" },
                values: new object[,]
                {
                    { "A1", 11, "Math", "Aleksandr", "Mixaylov" },
                    { "A2", 8, "History", "Nəzrin", "Əliyeva" },
                    { "B2", 10, "Physic", "Ramal", "Səmədzadə" }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Exams",
                columns: new[] { "Id", "BeginDate", "Score", "StudentId", "SubjectCode" },
                values: new object[] { 1, new DateTime(2021, 6, 9, 17, 7, 27, 346, DateTimeKind.Local).AddTicks(4209), 5, 1, "A1" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Exams",
                columns: new[] { "Id", "BeginDate", "Score", "StudentId", "SubjectCode" },
                values: new object[] { 2, new DateTime(2021, 5, 30, 17, 7, 27, 346, DateTimeKind.Local).AddTicks(4209), 9, 2, "A2" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Exams",
                columns: new[] { "Id", "BeginDate", "Score", "StudentId", "SubjectCode" },
                values: new object[] { 3, new DateTime(2021, 5, 20, 17, 7, 27, 346, DateTimeKind.Local).AddTicks(4209), 3, 3, "B2" });

            migrationBuilder.CreateIndex(
                name: "IX_Exams_StudentId",
                schema: "dbo",
                table: "Exams",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Exams_SubjectCode",
                schema: "dbo",
                table: "Exams",
                column: "SubjectCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Exams",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Students",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Subjects",
                schema: "dbo");
        }
    }
}
