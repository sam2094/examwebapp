﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;
using System;

namespace DataAccess.Migrations.Seed
{
    public class StudentSeed : BaseSeed
    {
        public static void Seed(EntityTypeBuilder<Student> builder)
        {
            builder.HasData(
                new Student()
                {
                    Id = 1,
                    Name = "Samir",
                    Surname = "Həsənov",
                    ClassNumber = 11
                },
                new Student()
                {
                    Id = 2,
                    Name = "Ruslan",
                    Surname = "Əhmədov",
                    ClassNumber = 10
                },
                new Student()
                {
                    Id = 3,
                    Name = "Ramiz",
                    Surname = "Hüseynov",
                    ClassNumber = 8
                }
            );
        }
    }
}
