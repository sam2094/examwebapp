﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;
using System;

namespace DataAccess.Migrations.Seed
{
    public class ExamSeed : BaseSeed
    {
        public static void Seed(EntityTypeBuilder<Exam> builder)
        {
            DateTime date = DateTime.Now;

            builder.HasData(
                new Exam()
                {
                    Id = 1,
                    StudentId = 1,
                    SubjectCode = "A1",
                    Score = 5,
                    BeginDate = date.AddDays(20)
                },
                new Exam()
                {
                    Id = 2,
                    StudentId = 2,
                    SubjectCode = "A2",
                    Score = 9,
                    BeginDate = date.AddDays(10)
                },
                new Exam()
                {
                    Id = 3,
                    StudentId = 3,
                    SubjectCode = "B2",
                    Score = 3,
                    BeginDate = date
                }
            );
        }
    }
}
