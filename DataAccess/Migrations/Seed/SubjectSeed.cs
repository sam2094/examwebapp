﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Entities;
using System;

namespace DataAccess.Migrations.Seed
{
    public class SubjectSeed : BaseSeed
    {
        public static void Seed(EntityTypeBuilder<Subject> builder)
        {
            builder.HasData(
                new Subject()
                {
                    Code = "A1",
                    Name = "Math",
                    TeacherName = "Aleksandr",
                    TeacherSurname = "Mixaylov",
                    ClassNumber = 11
                },
               new Subject()
               {
                   Code = "A2",
                   Name = "History",
                   TeacherName = "Nəzrin",
                   TeacherSurname = "Əliyeva",
                   ClassNumber = 8
               },
               new Subject()
               {
                   Code = "B2",
                   Name = "Physic",
                   TeacherName = "Ramal",
                   TeacherSurname = "Səmədzadə",
                   ClassNumber = 10
               }
            );
        }
    }
}
