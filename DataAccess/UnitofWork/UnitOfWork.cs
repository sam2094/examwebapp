﻿using DataAccess.Repositories.ExamRepository;
using DataAccess.Repositories.StudentRepository;
using DataAccess.Repositories.SubjectRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading.Tasks;

namespace DataAccess.UnitofWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool disposed = false;
        private readonly DbContext _dbContext;

        public IDbContextTransaction Transaction { get; set; }

        // Repositories
        private ISubjectRepository subjectRepository;
        private IStudentRepository studentRepository;
        private IExamRepository examRepository;

        // Repositories initialize
        public ISubjectRepository SubjectRepository() => subjectRepository = subjectRepository ?? new SubjectRepository(_dbContext);
        public IStudentRepository StudentRepository() => studentRepository = studentRepository ?? new StudentRepository(_dbContext);
        public IExamRepository ExamRepository() => examRepository = examRepository ?? new ExamRepository(_dbContext);

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region IUnitOfWork Members

        public void Begin()
        {
            Transaction = _dbContext.Database.BeginTransaction();
        }

        public async Task BeginAsync()
        {
            Transaction = await _dbContext.Database.BeginTransactionAsync();
        }

        public void Commit()
        {
            Transaction.Commit();
        }

        public async Task CommitAsync()
        {
            await Transaction.CommitAsync();
        }

        public void Rollback()
        {
            Transaction.Rollback();
        }

        public async Task RollbackAsync()
        {
            try
            {
                await Transaction.RollbackAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveChanges()
        {
            try
            {
                return _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IDisposable Members
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
