﻿using DataAccess.Repositories.ExamRepository;
using DataAccess.Repositories.StudentRepository;
using DataAccess.Repositories.SubjectRepository;
using System;
using System.Threading.Tasks;

namespace DataAccess.UnitofWork
{
    public interface IUnitOfWork : IDisposable
    {
        // Repositories
        ISubjectRepository SubjectRepository();
        IStudentRepository StudentRepository();
        IExamRepository ExamRepository();


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// 
        /// </summary>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// 
        /// </summary>
        void Begin();

        /// <summary>
        /// 
        /// </summary>
        Task BeginAsync();

        /// <summary>
        /// 
        /// </summary>
        void Commit();

        /// <summary>
        /// 
        /// </summary>
        Task CommitAsync();

        /// <summary>
        /// 
        /// </summary>
        void Rollback();

        /// <summary>
        /// 
        /// </summary>
        Task RollbackAsync();
    }
}
