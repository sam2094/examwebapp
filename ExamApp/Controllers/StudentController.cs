﻿using Microsoft.AspNetCore.Mvc;
using Models.Parameters.Student;
using Services.Services.StudentService;
using System.Threading.Tasks;
using Services.Services;
using System.Linq;

namespace ExamApp.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        public async Task<IActionResult> Index()
        {
            return await GetStudents();
        }


        [HttpPost]
        public async Task<IActionResult> Index(AddStudentInput input)
        {
            if (ModelState.IsValid)
            {
                ContainerResult<bool> result = await _studentService.AddStudent(input);

                if (!result.IsSuccess)
                {
                    TempData["Errors"] = result.ErrorList.Select(x => x.ErrorMessage).ToList();
                }
                else
                {
                    TempData["Success"] = "Student succesfully added";
                }

                return RedirectToAction();
            }
            else
            {
                await GetStudents();
                return View(input);
            }
        }


        private async Task<IActionResult> GetStudents()
        {
            ContainerResult<GetStudentsOutput> result = await _studentService.GetStudents();

            if (!result.IsSuccess)
            {
                return View();
            }

            ViewBag.Students = result.Output.Students;

            return View();
        }
    }
}
