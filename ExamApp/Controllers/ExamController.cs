﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Services.Services;
using System.Linq;
using Models.Parameters.Exam;
using Services.Services.ExamService;
using Models.Dtos;
using System.Collections.Generic;
using Services.Services.StudentService;
using Services.Services.SubjectService;
using Models.Parameters.Subject;
using Models.Parameters.Student;

namespace ExamApp.Controllers
{
    public class ExamController : Controller
    {
        private readonly IExamService _examService;
        private readonly IStudentService _studentService;
        private readonly ISubjectService _subjectService;

        public ExamController(IExamService examService, IStudentService studentService, ISubjectService subjectService)
        {
            _examService = examService;
            _studentService = studentService;
            _subjectService = subjectService;
        }

        public async Task<IActionResult> Index()
        {
            await FillSubjects();
            await FillStudents();
            return await GetExams();
        }


        [HttpPost]
        public async Task<IActionResult> Index(AddExamInput input)
        {
            if (ModelState.IsValid)
            {
                ContainerResult<bool> result = await _examService.AddExam(input);

                if (!result.IsSuccess)
                {
                    TempData["Errors"] = result.ErrorList.Select(x => x.ErrorMessage).ToList();
                }
                else
                {
                    TempData["Success"] = "Exam succesfully added";
                }

                return RedirectToAction();
            }
            else
            {
                await FillSubjects();
                await FillStudents();
                await GetExams();
                return View(input);
            }
        }


        private async Task<IActionResult> GetExams()
        {
            ContainerResult<GetExamsOutput> result = await _examService.GetExams();

            if (!result.IsSuccess)
            {
                return View();
            }

            ViewBag.Exams = result.Output.Exams;

            return View();
        }

        private async Task<IActionResult> FillSubjects()
        {
            ContainerResult<GetSubjectsOutput> result = await _subjectService.GetSubjects();

            if (!result.IsSuccess)
            {
                return View();
            }

            List<SubjectDto> subjects = result.Output.Subjects;

            ViewBag.Subjects = subjects;

            return View();
        }

        private async Task<IActionResult> FillStudents()
        {
            ContainerResult<GetStudentsOutput> result = await _studentService.GetStudents();

            if (!result.IsSuccess)
            {
                return View();
            }

            List<StudentDto> students = result.Output.Students;
            ViewBag.Students = students;
            return View();
        }
    }
}
