﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Services.Services;
using System.Linq;
using Services.Services.SubjectService;
using Models.Parameters.Subject;

namespace ExamApp.Controllers
{
    public class SubjectController : Controller
    {
        private readonly ISubjectService _subjectService;

        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        public async Task<IActionResult> Index()
        {
            return await GetSubjects();
        }


        [HttpPost]
        public async Task<IActionResult> Index(AddSubjecInput input)
        {
            if (ModelState.IsValid)
            {
                ContainerResult<bool> result = await _subjectService.AddSubject(input);

                if (!result.IsSuccess)
                {
                    TempData["Errors"] = result.ErrorList.Select(x => x.ErrorMessage).ToList();
                }
                else
                {
                    TempData["Success"] = "Subject succesfully added";
                }

                return RedirectToAction();
            }
            else
            {
                await GetSubjects();
                return View(input);
            }
        }


        private async Task<IActionResult> GetSubjects()
        {
            ContainerResult<GetSubjectsOutput> result = await _subjectService.GetSubjects();

            if (!result.IsSuccess)
            {
                return View();
            }

            ViewBag.Subjects = result.Output.Subjects;

            return View();
        }
    }
}
