﻿using Common.Enums;

namespace Common
{
    public sealed class Error
	{
		public ErrorHttpStatuses StatusCode { get; set; }
		public ErrorCodes ErrorCode { get; set; }
		public string ErrorMessage { get; set; }
	}
}
