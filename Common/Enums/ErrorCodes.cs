﻿namespace Common.Enums
{
    public enum ErrorCodes : int
    {
        SUCCESS = 0,
        INTERNAL_ERROR = 1,
        UNAUTHORIZED = 2,
        INPUT_IS_NOT_VALID = 3,
        SUBJECT_EXIST = 4,
        SUBJECT_DOES_NOT_EXIST = 5,
        STUDENT_DOES_NOT_EXIST = 6,
        STUDENT_AND_SUBJECT_CLASS_MUST_BE_SAME = 7
    }
}
