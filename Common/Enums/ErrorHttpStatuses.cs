﻿namespace Common.Enums
{
    public enum ErrorHttpStatuses : int
	{
		SUCCESS = 200,
		VALIDATION = 400,
		UNAUTHORIZED = 401,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		INTERNAL = 500,
	}
}
